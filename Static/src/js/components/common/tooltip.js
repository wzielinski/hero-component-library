var HERO_toolTip = {
		config: {
			$btn: $('.js-tooltip-btn'),
			$tooltipParent: $('.js-tooltip-parent'),
			$tooltip: $('.js-tooltip'),
			$closeBtn: $('.js-tooltip-close-btn'),
			tooltipOpen: false
		},

		init: function() {
			var _ = this;

			_.bindAria();
			_.bindEvents();
		},

		bindAria: function() {
			var _ = this;

			_.config.$tooltip.each(function(i) {
				var $this = $(this);

				_.setTooltipAttributes($this, _.config.$tooltip, i);
			});
		},

		bindEvents: function() {
			var _ = this;

			_.config.$btn.on('click', function(e) {
				e.preventDefault();
				e.stopPropagation();

				var $this = $(this);
						$tooltipParent = $(this).parents('.js-tooltip-parent')
						$tooltip = $(this).siblings('.js-tooltip');

				// Toggle tooltip
				_.config.tooltipOpen = !_.config.tooltipOpen;

					if(_.config.tooltipOpen) {
						console.log('visible');
						_.setTooltipVisible($tooltipParent, $tooltip);
					} else {
						console.log('hidden');
						_.setTooltipHidden($tooltipParent, $tooltip);
					}
			});

			_.config.$closeBtn.on('click', function(e) {
				_.config.$tooltipParent.removeClass('visible');
			})

			//stop the tooltip from being closed by clicking on it
			$('.js-tooltip').on('click', function(e) {
				e.stopPropagation();
			})

			if(_.config.$tooltip.length) {
				$(document).on('click', function() {
					_.setTooltipHidden($tooltipParent, $tooltip);
				});
			}
		},

		setTooltipAttributes: function($this, $tooltip, i) {
			var _ = this;

			$this.attr({
				'tabindex': -1,
				'aria-hidden': 'true'
			});
		},

		setTooltipVisible: function($tooltipParent, $tooltip) {
			$tooltipParent
					.addClass('visible')
					.siblings()
					.removeClass('visible');

			$tooltip.attr({
				'aria-hidden': 'false',
				'tabindex': '0'
			}).focus();
		},

		setTooltipHidden: function($tooltipParent, $tooltip) {
			$tooltipParent
					.removeClass('visible')
					.siblings()
					.removeClass('visible');

			$tooltip.attr({
				'tabindex': -1,
				'aria-hidden': 'true'
			});
		}
};

HERO_toolTip.init();
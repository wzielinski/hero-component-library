var HERO_dropdown = {
	config: {
		dropdownOpen: false,
		$dropdown: $('.js-dropdown'),
		$dropdownBtn: $('.js-dropdown-btn'),
		$dropdownList: $('.js-dropdown-list')
	},

	init: function() {
		_.setDefaultAriaAttributes();
		_.bindEvents();
	},

	setDefaultAriaAttributes: function($this, i) {

	}
}
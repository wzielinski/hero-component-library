var HERO_mediaCarousel = {
	config: {
		init: false,
		$carousel: $('.js-carousel'),
		$carouselNav: $('.media-carousel__nav')
	},

	init: function() {
		var _ = this;

		_.config.$carousel.each(function(i) {
			$this = $(this);
			$(this).attr('data-slider', i);

			$(this).slick({
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				fade: true,
				dots: false,
				prevArrow: '<button class="media-carousel__nav-btn media-carousel__nav-btn--prev slick-arrow" style=""><svg class="media-carousel__nav-icon"><title>Previous</title><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/Static/assets/images/sprites/vector.spritesheet.svg#icon-arrow-circle"></use></svg></button>',
				nextArrow: '<button class="media-carousel__nav-btn media-carousel__nav-btn--next slick-arrow" style=""><svg class="media-carousel__nav-icon"><title>Next</title><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/Static/assets/images/sprites/vector.spritesheet.svg#icon-arrow-circle"></use></svg></button>',
				appendArrows: $('[data-slider="' + i + '"] ~ .media-carousel__nav')
			});
		});

	}
};
var HERO_photoGallery = {
	config: {
		init: false,
		$carousel: $('.js-photo-gallery')
	},

	init: function() {
		var _ = this;

		_.config.$carousel.each(function(i) {
			$(this).attr('data-slider', i);

			var $this = $(this),
				$counter = $('[data-slider="' + i + '"] ~ .photo-gallery__counter');

			$this.on("init", function(event, slick){
				$counter.text(parseInt(slick.currentSlide + 1) + ' / ' + slick.slideCount);
			});

			$this.on("afterChange", function(event, slick, currentSlide){
				$counter.text(parseInt(slick.currentSlide + 1) + ' / ' + slick.slideCount);
			});

			$this.slick({
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				fade: true,
				dots: false,
				prevArrow: '<button class="photo-gallery__nav-btn photo-gallery__nav-btn--prev slick-arrow" style=""><svg class="photo-gallery__nav-icon"><title>Previous</title><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/Static/assets/images/sprites/vector.spritesheet.svg#icon-arrow-circle"></use></svg></button>',
				nextArrow: '<button class="photo-gallery__nav-btn photo-gallery__nav-btn--next slick-arrow" style=""><svg class="photo-gallery__nav-icon"><title>Next</title><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/Static/assets/images/sprites/vector.spritesheet.svg#icon-arrow-circle"></use></svg></button>',
				appendArrows: $('[data-slider="' + i + '"] ~ .photo-gallery__nav')
			});
		});

	}
};
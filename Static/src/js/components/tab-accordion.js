var HERO_tabAccordion = {
	config: {
		init: false,
		match: false,
		isAccordionView: false,
		isTabsView: false,
		$navlist: $('.js-navlist'),
		$tabContainer: $('.js-tab-container'),
		$tab: $('.js-tab'),
		$panels: $('.js-panels'),
		$panel: $('.js-panel')
	},

	init: function() {
		var _ = this;

		_.bindAria();
		_.mqContext();
		_.bindEvents();

	},

	bindAria: function() {
		var _ = this;

		$(_.config.$tabContainer).each(function(i){
			var $this = $(this);
			$this.attr('id', 'accordion_' + i);

			_.setAriaAttributes($this,_.config.$tab,_.config.$panel, i);
		});
	},

	setAriaAttributes: function($this, $tab, $panel, i) {
		var _ = this;

		_.config.$tabContainer
			.attr({
				//'role': 'tablist',
				'aria-multiselectable': 'false'
			});

		_.config.$tabContainer.find(_.config.$tab).each(function(j) {
			var $this = $(this);
			$this.attr({
				'id': 'a' + i + '_tab' + j,
				'role': 'tab',
				'tabindex': '0',
				'aria-controls': 'a' + i + '_panel' + j
			});
		});

		$this.find($panel).each(function(j) {
			var $this = $(this);
			$this.attr({
				'id': 'a' + i + '_panel' + j,
				'role': 'tabpanel',
				'tabindex': '0',
				'aria-hidden': 'true',
				'aria-labelledby': 'a' + i + '_tab' + j
			});
		});
	},

	bindEvents: function() {
		var _ = this;

		_.config.$navlist.on('keydown', 'li a', function (keyVent) {
			var arrows = [37, 38, 39, 40], //left arrow, up arrow, right arrow, down arrow
					which = keyVent.which,
					target = keyVent.target;

			if ($.inArray(which, arrows) > -1) {
				var adjacentTab = _.findAdjacentTab(target, _.config.$navlist, which);

				if (adjacentTab) {
					keyVent.preventDefault();
					adjacentTab.focus();
					// if desired behavior is that when tab recieves focus -> make it the active tab:
					_.setActiveAndInactive(adjacentTab, _.config.$navlist);
				}
			} else if (which === 13 || which === 32) { // ENTER |or| SPACE
				keyVent.preventDefault(); // don't scroll the page around...
				target.click();
			} else if (which === 34) { // PAGE DOWN
				keyVent.preventDefault(); // don't scroll the page
				var assocPanel = $('#' + this.getAttribute('aria-controls'));
				if (assocPanel) {
					assocPanel.focus();
				}
			}
		});

		$(document.body).on('keydown', '.panel', function (e) {
			if (e.which === 33) { // PAGE UP
				e.preventDefault(); // don't scroll
				var activeTab = $navlist.find('li.active a')[0];
				if (activeTab) {
					activeTab.focus();
				}
			}
		});

		// click support
		_.config.$navlist.on('click', 'li a', function (e) {
			_.setActiveAndInactive(this, _.config.$navlist);
		});

		$(".js-navlist li:first-child a").click();
	},

	findAdjacentTab: function(startTab, $list, key) {
		var dir = (key === 37 || key === 38) ? 'prev' : 'next';
		var adjacentTab = (dir === 'prev') ?
											$(startTab.parentNode).prev()[0] :
											$(startTab.parentNode).next()[0];

		if (!adjacentTab) {
			var allTabs = $list.find('li');
			if (dir === 'prev') {
				adjacentTab = allTabs[allTabs.length - 1];
			} else {
				adjacentTab = allTabs[0];
			}
		}

		return $(adjacentTab).find('a')[0];
	},

	setActiveAndInactive: function(newActive, $list) {
		$list.find('li').each(function () {
			var assocPanelID = $(this)
														.find('a')
														.first()
														.attr('aria-controls');

			var anchor = $(this).find('a')[0];

			if (this !== newActive.parentNode) {
				$(this).removeClass('active');
				anchor.tabIndex = -1;
				anchor.setAttribute('aria-selected', 'false');
				$('#' + assocPanelID)
					.removeClass('current')
					.attr('aria-hidden', 'true');
			} else {
				$(this).addClass('active');
				anchor.tabIndex = 0;
				anchor.setAttribute('aria-selected', 'true');
				$('#' + assocPanelID)
					.addClass('current')
					.removeAttr('aria-hidden');
			}
		});
	},

	mqContext: function() {
		var _ = this,
				mediaQuery = window.matchMedia('(min-width: 1224px)');

		//Add a listen event
		mediaQuery.addListener(mediaQueryChange);

		function mediaQueryChange(mediaQuery) {

			//Desktop
			if(mediaQuery.matches) {

				_.config.match = true;
				_.config.isAccordionView = false;
				_.config.isTabsView = true;

				//_.resetForDesktop();
				_.bindEventsDesktop();

			// Mobile / Tablet
			} else {

				_.config.match = false;
				_.config.isAccordionView = true;
				_.config.isTabsView = false;

				//_.resetForMobileTablet();
				_.bindEventsMobileTablet();
			}
		}

		//on page load
		mediaQueryChange(mediaQuery);
	},

	bindEventsMobileTablet: function() {
		var _ = this;
		console.log('run bind events mobile tablet');

		// switch to the accordion view
		_.config.$tabContainer
			.removeClass('tabs-view')
			.addClass('accordion-view');

		// fix the markup to be more suited for accordions
		_.config.$panels.find('.panel').each(function () {
			var panelID = this.id;
			var assocLink = panelID && $('.js-navlist a[aria-controls="' + panelID + '"]')[0];
			if (assocLink) {
				$(assocLink.parentNode).append(this);
			}
		});
	},

	bindEventsDesktop: function() {
		var _ = this;

    var wasAccordion = _.config.$tabContainer.hasClass('accordion-view');
    // switch to the tabs view
    _.config.$tabContainer
      .removeClass('accordion-view')
      .addClass('tabs-view');

    if (wasAccordion) {
      _.config.$navlist.find('.panel').each(function () {
        _.config.$panels.append(this);
      });
    }

	}
};
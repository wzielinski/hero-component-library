var gulp = require('gulp');

gulp.task('watch', function() {
    gulp.watch(`${global.skltr.sass}/**/*.scss`, gulp.series('sass:watch'));
    gulp.watch(`${global.skltr.vectors}/**/*.svg`, gulp.series('svg_sprite'));
    gulp.watch(`${global.skltr.jsSrc}/**/*.js`, gulp.series('scripts:dev', 'scripts:fallbacks'));
});

gulp.task('watch:bs', function() {
    gulp.watch(`${global.skltr.sass}/**/*.scss`, gulp.series('sass:watch:bs'));
    gulp.watch(`${global.skltr.vectors}/**/*.svg`, gulp.series('svg_sprite', 'browserSync:reload'));
	gulp.watch(`${global.skltr.jsSrc}/**/*.js`, gulp.series('scripts:dev', 'scripts:fallbacks',  'browserSync:reload'));
	gulp.watch(global.skltr.layouts, gulp.series('browserSync:reload'));
});
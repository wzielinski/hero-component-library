# HERO Component Library

Generic components can and should be added here as new ones are developed. The idea here is to maintain a comprehensive list of tested, fool-proof data/HTML structures and minor scaffolding CSS to have a shared vocabulary accross practices of HERO, namely between FED and XD.
const gulp = require('gulp');
global.skltr = require('./skeletor.config.js');

require('./skeletor.tasks/browserSync.js');
require('./skeletor.tasks/sass.js');
require('./skeletor.tasks/svg_sprite.js')
require('./skeletor.tasks/scripts.js');
require('./skeletor.tasks/watch.js');

// Default task, assumes you want all bells and whistles
gulp.task('default', gulp.parallel('browserSync', 'watch:bs'));

// Build task minifies and obfuscates all scripts and compiles css
gulp.task('build', gulp.series('scripts:prod', 'scripts:fallbacks', 'svg_sprite', 'sass'));

// Other tasks: 
// `watch`                (like default, but no browserSync),
// `scripts:dev`          (compiles js, leaves comments and console logs)
// `scripts:prod`         (compiles, obfuscates js, strips logs)
// `sass`                 (compiles sass)
// `svg_sprite`           (generates spritesheet from vectors)
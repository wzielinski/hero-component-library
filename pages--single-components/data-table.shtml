<!--#set var="PageTitle" value="Location Comparison Chart Component: Spaulding" -->

<!--#include virtual="/html_components/C01_header/C01_header--main.shtml" -->

<main role="main">
	<div class="row">
		<div class="content component col-md-12 rich-text">
			<h1 class="page-title page-title--static">Data Table</h1>
			<a href="/" class="cta cta--back-to-toc">Back to TOC</a>
			<p><a href="https://herodigital.atlassian.net/wiki/spaces/PH/pages/556105783/Location+Comparison+Chart+Component" target="_blank" class="cta">Wiki Documentation</a></p>

			<div class="data-table component">
				<div class="container">
					<div class="data-table__scroll-wrapper">
						<div class="data-table__scroll">
							<table class="data-table__table">
								<caption>
									<span class="visually-hidden">This is the summary for this data table! This is typically visually hidden.</span>
								</caption>
									<tr>
										<td colspan="4" class="data-table__header"><h2>Need help selecting the right Level of Care for you?</h2></td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th scope="col" id="rehab-hospital-boston" class="data-table__current-location"><span class="visually-hidden">Current Location:</span> Spaulding Rehabilitation Hospital Boston</th>
										<th scope="col" id="rehab-hospital-cape-cod">Spaulding Rehabilitation Hospital Cape Code</th>
										<th scope="col" id="eileen-outpatient-center-sandwich">Spaulding Eileen M. Ward Outpatient Center for Children Sandwich</th>
									</tr>
									<tr>
										<th id="levels-of-care" scope="row">Level of Care</th>
										<td headers="rehab-hospital-boston levels-of-care" class="data-table__current-location">Outpatient</td>
										<td headers="rehab-hospital-cape-cod levels-of-care">Inpatient</td>
										<td headers="eileen-outpatient-center-sandwich levels-of-care">Outpatient</td>
									</tr>
									<tr>
										<th id="overnight-stay-required" scope="row">Overnight Stay Required</th>
										<td headers="rehab-hospital-boston overnight-stay-required" class="data-table__current-location">No</td>
										<td headers="rehab-hospital-cape-cod overnight-stay-required">Yes</td>
										<td headers="eileen-outpatient-center-sandwich overnight-stay-required">No</td>
									</tr>
									<tr>
										<th id="patient-age-requirements" scope="row">Patient Age Requirements</th>
										<td headers="rehab-hospital-boston patient-age-requirements" class="data-table__current-location">15+ years old</td>
										<td headers="rehab-hospital-cape-cod patient-age-requirements">15+ years old *</td>
										<td headers="eileen-outpatient-center-sandwich patient-age-requirements">0-15 years old</td>
									</tr>
									<tr>
										<th id="therapy-intensity" scope="row">Level of Therapy Intensity</th>
										<td headers="rehab-hospital-boston therapy-intensity" class="data-table__current-location">1-2h per day, 1d per week, minimum</td>
										<td headers="rehab-hospital-cape-cod therapy-intensity">3h per day, 5d per week, minimum</td>
										<td headers="eileen-outpatient-center-sandwich therapy-intensity">1h per day, 1d per week, minimum</td>
									</tr>
									<tr>
										<th id="more-information-link" scope="row"><span class="visually-hidden">More Information link</span></th>
										<td headers="rehab-hospital-boston more-information-link" class="data-table__current-location"><a href="#" class="js-ga-location-comparison-table-link">Schedule an appointment<span class="visually-hidden"> at {{location}}</span></a></td>
										<td headers="rehab-hospital-cape-cod more-information-link"><a href="#" class="js-ga-location-comparison-table-link">View inpatient facilities<span class="visually-hidden"> at {{location}}</span></a></td>
										<td headers="eileen-outpatient-center-sandwich more-information-link"><a href="#" class="js-ga-location-comparison-table-link">View pediatric facilities<span class="visually-hidden"> at {{location}}</span></a></td>
									</tr>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</main>

<!--#include virtual="/html_components/C02_footer/_closing-body.shtml" -->